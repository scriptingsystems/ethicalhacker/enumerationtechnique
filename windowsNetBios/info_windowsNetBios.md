## NBTSTAT

> **nbtstat**, Mostrar estadísticas del protocolo NetBIOS sobre TCP/IP

A NetBIOS name is a unique 16 ASCII character string used to **identify the network devices** over TCP/IP; fifteen characters are used for the **device name**, and the sixteenth character is reserved for the **service or name record type**


[NetBIOS Name list](https://0xffsec.com/handbook/services/netbios/#netbios-suffixes-mcnab-nsa)


## NET

Net Help, muestra la información de ayuda de un comando, por ejemplo **net accounts** seria **net help accounts**
```powershell
PS C:\Users\basicuser> net help
The syntax of this command is:

NET HELP
command
     -or-
NET command /HELP

  Commands available are:

  NET ACCOUNTS             NET HELPMSG              NET STATISTICS
  NET COMPUTER             NET LOCALGROUP           NET STOP
  NET CONFIG               NET PAUSE                NET TIME
  NET CONTINUE             NET SESSION              NET USE
  NET FILE                 NET SHARE                NET USER
  NET GROUP                NET START                NET VIEW
  NET HELP

  NET HELP NAMES explains different types of names in NET HELP syntax lines.
  NET HELP SERVICES lists some of the services you can start.
  NET HELP SYNTAX explains how to read NET HELP syntax lines.
  NET HELP command | MORE displays Help one screen at a time.
```

```powershell
PS C:\Users\basicuser> net view \\10.10.1.22 /all
Shared resources at \\10.10.1.22



Share name  Type  Used as  Comment

-------------------------------------------------------------------------------
ADMIN$      Disk           Remote Admin
C$          Disk           Default share
IPC$        IPC            Remote IPC
NETLOGON    Disk           Logon server share
SYSVOL      Disk           Logon server share
The command completed successfully.
```


```powershell
PS C:\Users\basicuser> net config workstation
Computer name                        \\WINDOWS11
Full Computer name                   Windows11.ceh.com
User name                            basicuser

Workstation active on
        NetBT_Tcpip_{B56266B1-65C1-4867-B801-EE5B1FD95272} (000C294640A6)

Software version                     Windows 10 Enterprise Evaluation

Workstation domain                   CEH
Workstation Domain DNS Name          ceh.com
Logon domain                         CEH

COM Open Timeout (sec)               0
COM Send Count (byte)                16
COM Send Timeout (msec)              250
The command completed successfully.

PS C:\Users\basicuser> net config server
System error 5 has occurred.

Access is denied.
```



```powershell
PS C:\Users\basicuser> net group /domain
The request will be processed at a domain controller for domain ceh.com.


Group Accounts for \\Server2022.ceh.com

-------------------------------------------------------------------------------
*Cloneable Domain Controllers
*DnsUpdateProxy
*Domain Admins
*Domain Computers
*Domain Controllers
*Domain Guests
*Domain Users
*Enterprise Admins
*Enterprise Key Admins
*Enterprise Read-only Domain Controllers
*Group Policy Creator Owners
*Key Admins
*Protected Users
*Read-only Domain Controllers
*Schema Admins
The command completed successfully.
```

Muestra información de id de error dado
```powershell
PS C:\Users\basicuser> net file 2
System error 5 has occurred.

Access is denied.

PS C:\Users\basicuser> net helpmsg 5

Access is denied.
```


Mostrar los usuarios del dominio
```powershell
PS C:\Users\basicuser> net user /domain
The request will be processed at a domain controller for domain ceh.com.


User accounts for \\Server2022.ceh.com

-------------------------------------------------------------------------------
Administrator            basicuser                Guest
jason                    krbtgt                   martin
shiela
The command completed successfully.
```

