Attackers can perform manual LDAP enumaration using Python. Follow the steps given below to perform manual LDAP enumeration using Python
```bash
┌─[root@parrot]─[~]
└──╼ #python3
Python 3.9.2 (default, Feb 28 2021, 17:03:44) 
[GCC 10.2.1 20210110] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import ldap3
>>> server = ldap3.Server('10.10.1.22', get_info=ldap3.ALL, port=389)
>>> connection = ldap3.Connection(server)
>>> connection.bind()
True
>>> server.info
DSA info (from DSE):
  Supported LDAP versions: 3, 2
  Naming contexts: 
    DC=ceh,DC=com
    CN=Configuration,DC=ceh,DC=com
    CN=Schema,CN=Configuration,DC=ceh,DC=com
    DC=DomainDnsZones,DC=ceh,DC=com
    DC=ForestDnsZones,DC=ceh,DC=com
```





Attackers use ldapsearch for enumerating AD users. It allows attackers to establish a connection with an LDAP server to perform different searches using specific filters

The following command can be executed to obtain additional details related to the naming contexts:
```bash
┌─[root@parrot]─[~]
└──╼ #ldapsearch -h 10.10.1.22 -x -s base namingcontexts
# extended LDIF
#
# LDAPv3
# base <> (default) with scope baseObject
# filter: (objectclass=*)
# requesting: namingcontexts 
#

#
dn:
namingcontexts: DC=ceh,DC=com
namingcontexts: CN=Configuration,DC=ceh,DC=com
namingcontexts: CN=Schema,CN=Configuration,DC=ceh,DC=com
namingcontexts: DC=DomainDnsZones,DC=ceh,DC=com
namingcontexts: DC=ForestDnsZones,DC=ceh,DC=com

# search result
search: 2
result: 0 Success

# numResponses: 2
# numEntries: 1
```
